export const clientDomain: any = process.env.REACT_APP_CLIENT_DOMAIN;
export const gatewayUri =
  process.env.REACT_APP_GATEWAY_URI || 'http://localhost:4000';
