import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { Home, NotFound,Login,Accueil } from './views';

const Routes = () => {
  return (
    <Switch>
      <Redirect exact from="/" to="/login" />
      <Route exact path="/home" component={Home} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/accueil" component={Accueil} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default Routes;
