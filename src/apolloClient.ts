import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { gatewayUri } from './config';

import {setContext} from '@apollo/client/link/context';

const httpLink=createHttpLink({
  uri: gatewayUri
});

const authLink = setContext((_,{headers})=>{
  const token = localStorage.getItem('token');
  return{
    headers:{
      ...headers,
      authorization:token?`Bearer ${token}` : ""
    }
  }
})

export const createAppolloClient = () => {
  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
  });

  return client;
};
