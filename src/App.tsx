import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter as Router, useLocation } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Routes from './Routes';
import { createAppolloClient } from './apolloClient';
import theme from './theme';
import { AuhtProvider } from './views/context/context';
import { MessageProvider } from './views/context/message';

const client = createAppolloClient();

function App() {
  return (
    <ApolloProvider client={client}>
      <AuhtProvider>
        <MessageProvider>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Router>
              <ScrollToTop />
              <Routes />
            </Router>
          </ThemeProvider>
        </MessageProvider>
      </AuhtProvider>
    </ApolloProvider>
  );
}

const ScrollToTop = () => {
  const { pathname } = useLocation();

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};

export default App;
