import React, { Fragment} from 'react';
import { Row, Button } from "react-bootstrap"
import { Link, useHistory } from 'react-router-dom';
import { useAuthDispatch } from '../context/context';
import User from '../User/User';
import Message from '../Message/Message'



const Accueil: React.FC = () => {

  const dispatch = useAuthDispatch();
  const goPageLogin = useHistory();


  

  const logout = () => {
    dispatch({
      type: 'LOGOUT',
    })
     window.location.href="/login"

    // goPageLogin.push({
    //   pathname: "/login"
    // })
  }



  

  return (
    <Fragment>
      <Row className="bg-white justify-content-around">
        <Link to="/login">
          <Button variant="link">Login</Button>
        </Link>
        <Link to="/home">
          <Button variant="link">Register</Button>
        </Link>

        <Button variant="link" onClick={logout}>Logout</Button>

      </Row>

      <Row className="bg-white">
      <User />
       <Message />
      </Row>
    </Fragment>


  );
};

export default Accueil;
