import React, { Fragment, useEffect, useState } from 'react';
import { gql, useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { Row, Col, Button, Image, Form } from "react-bootstrap"
import { useMessageDispatch, useMessageState } from '../context/message'

import SingleMessage from "./SingleMessage"

const GET_MESSAGE = gql`
query getMessage($from:String!){
   getMessage(from:$from){uuid content from to createdAt}
  }

`;


const SEND_MESSAGE = gql`
mutation sendMessage($to:String! $content:String!){
  sendMessage(to:$to content:$content){
    uuid content from to createdAt
  }
}

`;



export default function Message() {
  const dispatch = useMessageDispatch();
  const { users } = useMessageState()

  const userSelected = users?.find(u => u.selected == true)
  const messages = userSelected?.messages
  const [content, setContent] = useState('')

  const [getMessage, { loading: messageLoading, data: messagesData }] = useLazyQuery(GET_MESSAGE)

  useEffect(() => {

    if (userSelected && !userSelected.messages) {
      getMessage({ variables: { from: userSelected.username } })
    }

  }, [userSelected])


  useEffect(() => {
    if (messagesData) {
      dispatch({
        type: "SET_USER_MESSAGE", payload: {
          username: userSelected.username,
          messages: messagesData.getMessage
        }
      })
    }
  }, [messagesData])


  const [sendMessage] = useMutation(SEND_MESSAGE, {
    onCompleted: data => dispatch({
      type: 'ADD_MESSAGE', payload: {
        username: userSelected.username,
        message: data.sendMessage
      }
    }),
    onError: err => console.log(err)
  })

  const submitMessage = e => {
    e.preventDefault()

    if (content.trim() == '' || !userSelected) return
    setContent('')
    sendMessage({ variables: { to: userSelected.username, content } })
  }

  let selectChatMarkup

  if (!messages && !messageLoading) {
    selectChatMarkup = <p className="info-text">Select a Friend</p>
  } else if (messageLoading) {
    selectChatMarkup = <p className="info-text">Loading ...</p>
  } else if (messages.length > 0) {
    selectChatMarkup = messages.map((message, index) => (
      <Fragment key={message.uuid}>
        <SingleMessage message={message} />
        {
          index == messages.length - 1 && (
            <div className="invisible">
              <hr className="m-0" />
            </div>
          )
        }
      </Fragment>




    ))
  } else if (messages.length == 0) {
    selectChatMarkup = <p className="info-text">Your are connected! Say hello</p>
  }

  return (
    <Col xs={10} md={8} >
      <div className="message-box d-flex flex-column-reverse">
        {selectChatMarkup}
      </div>
      <div>
        <Form onSubmit={submitMessage}>
          <Form.Group>
            <Form.Control
              type="text"
              className="message-input rounded-pill p-4 bg-secondary border-0"
              placeholder="Type message..."
              value={content}
              onChange={e => setContent(e.target.value)}
            />
          </Form.Group>
        </Form>
      </div>


    </Col>
  );
};

// export default Message;
