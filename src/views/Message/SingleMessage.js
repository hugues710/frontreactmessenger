import React from 'react';
import classNames from 'classnames'
import { useAuthState } from "../context/context"
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import moment from 'moment'

function SingleMessage({ message }) {

    console.log(useAuthState());
    const { user } = useAuthState()

    const sent = message.from == user.username

    const receive = !sent
    return (
        <OverlayTrigger
            placement={!sent ? "left" : "bottom"}
            overlay={
                <Tooltip >
                    {moment(message.createdAt).format("MMMM DD, YYYY @ h:mm a")}
                </Tooltip>
            }
            transition={false}
            >

            <div className={classNames("d-flex my-3", {
                'ml-auto': sent,
                'mr-auto': receive
            })}>
                <div className={classNames('py-2 px-3 rounded-pill', {
                    'bg-primary': sent,
                    'bg-secondary': receive
                })}>
                    <p className={classNames({
                        'text-white': sent
                    })} key={message.uuid}>{message.content}</p>
                </div>
            </div>

        </OverlayTrigger>



    );
}

export default SingleMessage; 