import React, { useState } from 'react';
import { Row, Col, Form, Button } from "react-bootstrap"
import { gql, useLazyQuery } from '@apollo/client'
import { Link, useHistory } from 'react-router-dom';

import {useAuthDispatch} from '../context/context'

const LOGIN_USER = gql`
    query login($username:String! $password:String!){
        login(username:$username password:$password){
            username email token
        }
    }
`;

const Login: React.FC = (pop) => {

    const [variables, setVariable] = useState({
        username: '',
        password: ''
    })
    const pageRegister = useHistory();

    const dispatch =useAuthDispatch();

    const [login_user, { loading }] = useLazyQuery(LOGIN_USER, {
        onError: (err) => {
            console.log(err);

        },
        onCompleted: (data) => {
          
            dispatch({
                type : "LOGIN",
                payload:data.login
            })
            pageRegister.push({
                pathname:"/accueil"
            })
        }
    });

    const seLogger = (e: any) => {
        e.preventDefault()

        login_user({ variables })

    };
    return (
        <Row className="bg-white p-5 justify-content-center">
            <Col sm={6} md={6} lg={4}>
                <h1 className="text-center">Authentification</h1>

                <Form onSubmit={e => seLogger(e)}>

                    <Form.Group>
                        <Form.Label>Nom :</Form.Label>
                        <Form.Control type="text" placeholder="Entrer nom" value={variables.username} onChange={e => setVariable({ ...variables, username: e.target.value })} />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Mot de Passe</Form.Label>
                        <Form.Control type="password" placeholder="Entrer mot de passe" value={variables.password} onChange={e => setVariable({ ...variables, password: e.target.value })} />
                    </Form.Group>

                    <div className="text-center">
                        <Button variant="success" type="submit">
                            {loading ? 'Chargement...' : 'Se Connecter'}
                        </Button>
                        <br />
                        <small>Pas de compte ? <Link to="/home">Créer Compte</Link></small>
                    </div>

                </Form>
            </Col>
        </Row>
    );
}

export default Login;