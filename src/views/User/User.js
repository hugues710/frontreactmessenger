import React from 'react';
import {  gql, useQuery } from '@apollo/client';
import { Col, Image } from "react-bootstrap"
import { useMessageDispatch, useMessageState } from '../context/message'
import classNames from 'classnames'

import img from '../../img.jpg'

const GET_USER = gql`
 query getUsers{
  getUsers{
    username createdAt
    latestMessage {uuid from to content createdAt}
  }
 }
`;

export default function User() {
    const dispatch = useMessageDispatch();
    const {users} = useMessageState()

  const userSelected =users?.find(u => u.selected == true)?.username


    const { loading } = useQuery(GET_USER, {
        onCompleted: data => dispatch({ type: "SET_USERS", payload: data.getUsers }),
        onError: err => console.log(err)
    });


    let userMarkup;

    if (!users || loading) {
        userMarkup = <p>Loading ...</p>
    } else if (users.length == 0) {
        userMarkup = <p>No users have joined yet</p>
    } else {
        userMarkup = users.map((u) => {
            const selectUser= userSelected == u.username
          return  (<div role="button" key={u.username} className={classNames("d-flex  p-3 ",{'bg-white':selectUser})} onClick={() => dispatch({type:'SET_SELECTED_USER',payload : u.username})}>
                <Image src={img} roundedCircle style={{ width: 50, height: 50, objectFit: 'cover' }} />
                <div className="d-none d-md-block">
                    <p className="text-success">{u.username}</p>
                    <p className="font-weight-light">
                        {u.latestMessage ? u.latestMessage.content : "You are connected"}
                    </p>
                </div>
            </div>)
        })
    }
    return (
        <Col xs={2} md={4} className="p-0 bg-secondary" >
            {userMarkup}
        </Col>
    );
}

// export default User;
