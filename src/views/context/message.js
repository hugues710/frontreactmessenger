import React, { createContext, useReducer, useContext } from 'react';


const MessageStateContext = createContext();
const MessageDisptchContext = createContext();

let user = null;


const messageReducer = (state, action) => {
    let userCopy,userIndex
    const {username,message,messages} =action.payload
      
    switch (action.type) {
        case "SET_USERS":
            return {
                ...state,
                users: action.payload
            }
            case "SET_USER_MESSAGE":
               
                userCopy = [...state.users]

                 userIndex = userCopy.findIndex(u => u.username === username)
                userCopy[userIndex] = {...userCopy[userIndex],messages}
                return {
                    ...state,
                    users : userCopy
                }
            case 'SET_SELECTED_USER':
                 userCopy = state.users.map((user)=>({
                    ...user,
                    selected:user.username == action.payload
                }))
                return {
                    ...state,
                    users : userCopy
                }

                case 'ADD_MESSAGE' :
                    userCopy = [...state.users]

                     userIndex = userCopy.findIndex(u => u.username === username)

                     let newUser={
                         ...userCopy[userIndex],
                         messages:[message,...userCopy[userIndex].messages]
                     }
                     userCopy[userIndex] = newUser

                     return {
                        ...state,
                        users : userCopy
                    }
        default:
            throw new Error('Unknown action type')

    }
};

export const MessageProvider = ({ children }) => {
    const [state, dispatch] = useReducer(messageReducer, { users: null });
    return (
        <MessageDisptchContext.Provider value={dispatch}>
            <MessageStateContext.Provider value={state}>
                {children}
            </MessageStateContext.Provider>
        </MessageDisptchContext.Provider>
    )


};

export const useMessageState = () => useContext(MessageStateContext)
export const useMessageDispatch = () => useContext(MessageDisptchContext)