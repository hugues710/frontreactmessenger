import React, { createContext, useReducer, useContext } from 'react';
import jwtDecode from 'jwt-decode';

const AuthStateContext = createContext();
const AuthDisptchContext = createContext();


let user = null;

const token = localStorage.getItem('token');
if (token) {
    const tokenDecode = jwtDecode(token);
    const expireAt = new Date(tokenDecode.exp * 1000);
    if (new Date() > expireAt) {
        localStorage.removeItem("token");
    } else {
        user = tokenDecode;
    }
} else {
    console.log("no token found");
}

const auhtReducer = (state, action) => {
    switch (action.type) {
        case 'LOGIN':
            localStorage.setItem("token", action.payload.token)
           
            break;
        case 'LOGOUT':
            localStorage.removeItem("token")
            return {
                ...state,
                user: null
            }

        default:
            throw new Error('Unknown action type')

    }
};

export const AuhtProvider = ({ children }) => {
    const [state, dispatch] = useReducer(auhtReducer, { user });
    return (
        <AuthDisptchContext.Provider value={dispatch}>
            <AuthStateContext.Provider value={state}>
                {children}
            </AuthStateContext.Provider>
        </AuthDisptchContext.Provider>
    )


};

export const useAuthState = () => useContext(AuthStateContext)
export const useAuthDispatch = () => useContext(AuthDisptchContext)