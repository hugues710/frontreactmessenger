export { default as NotFound } from './NotFound';
export { default as Home } from './Home';
export { default as Register } from './Register';
export { default as Login } from './Login';
export { default as Accueil } from './Accueil';
