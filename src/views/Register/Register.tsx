import React, { useState } from 'react';
import { Row, Col, Form, Button } from "react-bootstrap"
import {gql,useMutation} from '@apollo/client'
import { Link, useHistory } from 'react-router-dom';

const REGISTER_USER =gql`
    mutation register($username:String! $email:String! $password:String! $confirmPassword:String!){
        register(username:$username email:$email password:$password confirmPassword:$confirmPassword){
            username email createdAt
        }
    }
`;

const Register: React.FC = (props) => {

    const [variables, setVariable] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: ''
    })

    const pageLogin = useHistory();

    const [register_user,{loading}] = useMutation(REGISTER_USER,{
        update:(_,__)=>{

            pageLogin.push({
                pathname:"/login"
            })
            
        },
        onError:(err)=>{
            console.log(err);
            
        }
    });

    const enregistrerCompte = (e: any) => {
        e.preventDefault()

        register_user({variables})
      

    };
    return (
        <Row className="bg-white p-5 justify-content-center">
            <Col sm={6} md={6} lg={4}>
                <h1 className="text-center">Création Compte</h1>

                <Form onSubmit={e => enregistrerCompte(e)}>

                    <Form.Group>
                        <Form.Label>Nom :</Form.Label>
                        <Form.Control type="text" placeholder="Entrer nom" value={variables.username} onChange={e => setVariable({ ...variables, username: e.target.value })} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Email :</Form.Label>
                        <Form.Control type="email" placeholder="Entrer email" value={variables.email} onChange={e => setVariable({ ...variables, email: e.target.value })} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Mot de Passe</Form.Label>
                        <Form.Control type="password" placeholder="Entrer mot de passe" value={variables.password} onChange={e => setVariable({ ...variables, password: e.target.value })} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Confirmer Mot de Passe</Form.Label>
                        <Form.Control type="password" placeholder="Retaper mot de passe" value={variables.confirmPassword} onChange={e => setVariable({ ...variables, confirmPassword: e.target.value })} />
                    </Form.Group>
                    <div className="text-center">
                        <Button variant="success" type="submit">
                           {loading ? 'Chargement...' : 'Créer compte'}
              </Button>
              <br/>
                    <small>Déjà un compte ? <Link to="/login">Se connecter</Link></small>
                    </div>
                    
                </Form>
            </Col>
        </Row>
    );
}

export default Register;