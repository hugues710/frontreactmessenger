import React from 'react';
import './Home.scss'
import { Container } from "react-bootstrap"
import Register from '../Register/Register'

const Home: React.FC = () => {
  
  return (
    <Container>
      <Register />
    </Container>
  );
};

export default Home;
