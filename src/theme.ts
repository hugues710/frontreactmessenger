import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: 'rgba(15,35,104,1.0)',
    },
    secondary: {
      main: 'rgba(255,74,74,1.0)',
    },
  },
});

export default theme;
