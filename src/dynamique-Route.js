import React, { Component } from 'react';
import { useAuthState } from './views/context/context';
import { Redirect, Route, Switch } from 'react-router-dom';

export default function DynamiqueRoute(props) {

    const { user } = useAuthState();

    if (props.authenticated && !user) {
        console.log("here")
        return <Redirect to="/login" />
    } else if (props.guest && user) {
        console.log("here 2");
        return <Redirect to="/accueil" />
    } else {
        return <Route component={props.component} {...props} />
    }
};

